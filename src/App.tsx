import React, { useState, useEffect } from 'react';
import Board from './Components/Board';
import Modal from './Components/Modal';
import Icon1 from './static/images/icon-square-big 2.svg';
import Icon2 from './static/images/5f34fd1aa2ebfaf2cd548bafeb021c8f.svg';
import Icon3 from './static/images/1280px-React-icon.svg';
import Icon4 from './static/images/5848309bcef1014c0b5e4a9a 1.svg';
import Icon5 from './static/images/Nginx-featured-logo 3.svg';
import Icon6 from './static/images/nodejs-logo-FBE122E377-seeklogo 2.svg';
import Icon7 from './static/images/Typescript_logo_2020.svg';
import Icon8 from './static/images/webstorm-icon-logo-black-and-white 2.svg';

const CARDS_AMOUNT = 16; 

const iconUrls = [
  Icon1, Icon2, Icon3, Icon4, Icon5, Icon6, Icon7, Icon8
]

const shuffleCards = (cards: Array<{ open: boolean, hidden: boolean, id: number, value: string, icon: string  }>) => {
  let shuffledCards = cards
  for (let i = shuffledCards.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledCards[i], shuffledCards[j]] = [shuffledCards[j], shuffledCards[i]];
  }

  return shuffledCards
}  

const App = () => {
  const [cards, updateCards] = useState(
    shuffleCards(Array(2).fill(Array.from(Array(CARDS_AMOUNT/2).keys()))
      .flat()
      .map((c, i) => ({ id: i, open: false, value: c, icon: iconUrls[c], hidden: false })))
  )

  const [moves, updateMoves] = useState(40)
  const [modal, updateModal] = useState({ open: false, status: 'loose' })



  /* React.useEffect(() => {
    const shuffledCards = shuffleCards(cards)
  }, []) */


  const closeTimeout = (updatedCards: Array<any>) => setTimeout(() => closeCards(updatedCards), 3000)

  const handleClick = (id: Number, value: String) => {
    const currentMoves = moves - 1
    updateMoves(currentMoves)

    let updatedCards = cards.map((c) => c.id !== id ? c : {...c, open: true})
    updateCards(updatedCards)

    const openCards = cards.filter((c) => c.open)
    const sameValueCard = openCards.filter((c) => c.value === value && c.id !== id)

    if (openCards.length === 1 && sameValueCard.length > 0) {
      updatedCards = updatedCards.map((c) => c.value !== value ? c : {...c, hidden: true, open: false})
      updateCards(updatedCards)
      return
    } 
    if (openCards.length === 1 && sameValueCard.length <= 0) {
      closeTimeout(updatedCards)
    } else if (openCards.length > 1) {
      closeCards(updatedCards)
    }
    checkMovesAmount(currentMoves, updatedCards)
  }

  const checkMovesAmount = (currentMoves: number, updatedCards: Array<{ hidden: boolean }>) => {
    if (currentMoves <= 0) {
      updateModal({ open: true, status: 'loose' })
    } else if (updatedCards.filter(c => c.hidden).length >= updatedCards.length) {
      updateModal({ open: true, status: 'win' })
    }
  }

  const closeCards = (updatedCards: Array<any>) => {
    console.log(updatedCards)
    updateCards(updatedCards.map((c) => c.hidden ? c : ({ ...c, open: false })))
  }

  const handleButtonClick = (e: any) => {
    e.preventDefault;
    // shuffleCards(cards, updateCards)
  }


  return (
    <div>
      <header>
        <h1 className='title'>
          Memory
        </h1>
      </header>
      <div className='wrapper'>
        <div className='text_container'>
          <p className='text'>Сделано ходов</p>
          <p className='text text_value'>{ 40 - moves }</p>
        </div>
        <Board cards={cards} onClick={handleClick}/>
        <div className='text_container'>
          <p className='text'>Ходов осталось</p>
          <p className='text text_value'> { moves } </p>
        </div>
      </div>
      <Modal moves={40 - moves} status={modal.status} onClick={handleButtonClick} open={modal.open}/>
    </div>
  );
}

export default App;
