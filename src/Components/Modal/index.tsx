import React, { FC } from 'react';
import Button from '../Button';
import './Modal.css';

type Modal = {
    open: boolean,
    status: string,
    moves: number,
    onClick?: React.MouseEventHandler<HTMLButtonElement>
}

const Modal: FC<Modal> = ({ open, status, moves, onClick }) => {
    return (
        <div className={`modal${open ? ' modal_open' : ''}`}>
            <p className='modal_text'>
                {
                    status === 'win' ? `Ура, ВЫ выиграли! это заняло ${moves} ходов` : 'УВЫ, ВЫ ПРОИГРАЛИ У ВАС КОНЧИЛИСЬ ХОДЫ'
                }
            </p>
            <Button onClick={onClick}>
                сыграть еще
            </Button>
        </div>
    );
  }

export default Modal;