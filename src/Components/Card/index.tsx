import React, { FC } from 'react';
import './Card.css';

type Card = {
  onClick?: (id: number, value: string) => void,
  card: { open: boolean, hidden: boolean, id: number, value: string, icon: string  }
}

const Card: FC<Card> = ({ onClick, card }) => {
    return (
      <div 
        className={`card${card.open ? ' card_open' : ''}${card.hidden ? ' card_hidden' : ''}`} 
        onClick={() => onClick && onClick(card.id, card.value)}
      >
        {
            card.open && <img src={card.icon}/>
        }
      </div>
    );
}

export default Card;