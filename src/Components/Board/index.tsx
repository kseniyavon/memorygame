import Card from '../Card';
import React, { FC } from 'react';
import './Board.css';

type Board = {
  cards: Array<any>,
  onClick: (id: number, value: string) => void
}

const Board: FC<Board> = ({ cards, onClick }) => {
    return (
      <div className="board">
        {
            cards.map((card, i) => (
                <Card card={card} key={card.id} onClick={onClick}/>
            ))
        }
      </div>
    );
  }

export default Board;