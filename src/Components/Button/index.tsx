import React, { FC } from 'react';
import './Button.css';

type Button = {
    onClick?: React.MouseEventHandler<HTMLButtonElement>,
    children: string | JSX.Element | JSX.Element[]
}

const Button: FC<Button> = ({ onClick, children }) => {
    return (
        <button className='button' onClick={onClick}>
            { children }
        </button>
    );
  }

export default Button;